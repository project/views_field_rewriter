<?php

/**
 * @file
 * Definition of views_field_rewriter_handler_custom_field.
 */

/**
 * Provides a custom views field.
 *
 * This takes the value from another field in the view and outputs either
 * "0" or "1" depending on the value of that other field.
 */
class views_field_rewriter_handler_custom_field extends views_handler_field {

  /**
   * Overridden views_handler_field::options_defintion.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['fieldname'] = array('default' => '');
    $options['matches_0'] = array('default' => '');
    $options['matches_1'] = array('default' => '');
    $options['default_output'] = array('default' => '');
    return $options;
  }

  /**
   * Overridding views_handler_field::options_form.
   *
   * This allows the user to select which field to rewrite as 1/0.
   */
  function options_form(&$form, &$form_state) {
    $options = array();
    foreach ($this->view->get_items('field') as $fieldname => $field_info) {
      $options[$fieldname] = $field_info['label'];
    }

    parent::options_form($form, $form_state);

    $form['fieldname'] = array(
      '#type' => 'select',
      '#title' => t('Field name'),
      '#options' => $options,
      '#description' => t('Name of field to take as an input.'),
      '#default_value' => $this->options['fieldname'],
    );

    $form['matches_0'] = array(
      '#type' => 'textarea',
      '#title' => t('Values that will output 0'),
      '#description' => t('Any values matching these will make this field output 0.'),
      '#rows' => 5,
      '#cols' => 60,
      '#default_value' => $this->options['matches_0'],
    );

    $form['matches_1'] = array(
      '#type' => 'textarea',
      '#title' => t('Values that will output 1'),
      '#description' => t('Any values matching these will make this field output 1.'),
      '#rows' => 5,
      '#cols' => 60,
      '#default_value' => $this->options['matches_1'],
    );

    $form['default_output'] = array(
      '#type' => 'select',
      '#title' => t('Default if no matches'),
      '#description' => t('The value to output if the selected field returns no output'),
      '#options' => array('0' => '0', '1' => '1'),
      '#default_value' => $this->options['default_output'],
    );
  }

  /**
   * Overridden method views_handler_field::query.
   *
   * Need this for non-table based fields as otherwise the parent's
   * implementation will be used which assumes a table.
   */
  function query() {
  }

  /**
   * Overridden method views_handler_field::render.
   *
   * Outputs 1 or 0 depending on whether the field specified
   * by the option 'fieldname' matches one of the options in
   * either matches_for_0 or matches_for_1.
   */
  function render($data) {
    $output = $this->options['default_output'];
    if (!empty($this->options['fieldname'])) {
      $fieldname = "field_" . $this->options['fieldname'];
      if (isset($data->{$fieldname})) {
        $field_value = $data->{$fieldname}[0]['raw']['value'];

        if (preg_match("/^$field_value\$/", $this->options['matches_0'])) {
          $output = "0";
        }
        elseif (preg_match("/^$field_value\$/", $this->options['matches_1'])) {
          $output = "1";
        }
      }
    }
    return $output;
  }

}
