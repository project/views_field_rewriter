<?php

/**
 * Implementation of hook_views_handlers().
 */
function views_field_rewriter_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_field_rewriter'),
    ),
    'handlers' => array(
      'views_field_rewriter_handler_custom_field' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
 
/**
 * Implements hook_views_data().
 */
function views_field_rewriter_views_data() {
  $data = array();

  $data['rewriter']['table']['group'] = t('Field Rewriter');
  $data['rewriter']['table']['join'] = array(
    '#global' => array(),
  );

  $data['rewriter']['views_field_rewriter_handler_custom_field'] = array(
    'title' => t('Field Rewriter'),
    'help' => t('This takes the value from one field and outputs a "0" or "1" depending on the value of the other field.'),
    'field' => array(
      'handler' => 'views_field_rewriter_handler_custom_field',
    ),
  );
  return $data;
}
